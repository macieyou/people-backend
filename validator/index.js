exports.createPostValidator = (req, res, next) => {
     // title
    req.check('title', "Napisz tytuł").notEmpty();
    req.check('title', "Tytuł musi zawierać od 4 do 150 znaków").isLength({
        min: 4,
        max: 150
    });

    // body
    req.check('body', "Napisz treść").notEmpty();
    req.check('body', "Treść musi mieć od 4 do 2000 znaków").isLength({
        min: 4,
        max: 2000
    });

    // check for errors
    const  errors = req.validationErrors();
    // if error show the first one as they happen
    if (errors) {
        const firstError = errors.map((error) => error.msg)[0];
        return res.status(400).json({error: firstError});
    }
    // proceed to next middleware
    next();
};

exports.userSignupValidator = (req, res, next) => {
    // name is not null and between 4-10 characters
    req.check("name", "Imię jest wymagane").notEmpty();
    // email is not null, it is valid and normalized
    req.check("email", "Adres e-mail musi zawierać od 3 do 32 znaków")
        .matches(/.+\@.+\..+/)
        .withMessage("Adres e-mail musi zawierać @")
        .isLength({
            min: 4,
            max: 2000
        });
    // check for password
    req.check("password", "Hasło wymagane jest").notEmpty();
    req.check("password")
        .isLength({min: 6})
        .withMessage("Hasło musi zawierać co najmniej 6 znaków")
        .matches(/\d/)
        .withMessage("Hasło musi zawierać cyfrę");
    // check for errors
    const  errors = req.validationErrors();
    // if error show the first one as they happen
    if (errors) {
        const firstError = errors.map((error) => error.msg)[0];
        return res.status(400).json({error: firstError});
    }
    // proceed to next middleware
    next();
};

exports.passwordResetValidator = (req, res, next) => {
    // check for password
    req.check("newPassword", "Hasło wymagane jest").notEmpty();
    req.check("newPassword")
        .isLength({ min: 6 })
        .withMessage("Hasło musi zawierać co najmniej 6 znaków")
        .matches(/\d/)
        .withMessage("Hasło musi zawierać cyfrę");

    // check for errors
    const errors = req.validationErrors();
    // if error show the first one as they happen
    if (errors) {
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({ error: firstError });
    }
    // proceed to next middleware or ...
    next();
};