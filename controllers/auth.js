const _ = require("lodash");
const { sendEmail } = require("../helpers");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const expressJwt = require('express-jwt');
const User = require('../models/user');

exports.signup = async (req, res) => {
    const userExists = await User.findOne({email: req.body.email});
    if(userExists)
        return res.status(403).json({
            error: "Adres e-mail jest zajęty!"
        });
    const user = await new User(req.body);
    await user.save();
    res.status(200).json({message: "Rejestracja udana! Proszę się zalogować."});
};

exports.signin = (req, res) => {
    // find the user based on email
    const {email, password} = req.body;
    User.findOne({email}, (err, user) => {
        // if err or no user
        if (err || !user) {
            return res.status(401).json({
                error: "Użytkownik z tym adresem e-mail nie istnieje. Proszę się zarejestrować."
            });
        }
        // if user is found make sure the email and password match
        // create authenticate method in model and use here
        if (!user.authenticate(password)) {
            return res.status(401).json({
                error: "Adres e-mail i hasło nie są zgodne"
            });
        }
        // generate a token with user id and secret
        const token = jwt.sign({_id: user.id, role: user.role}, process.env.JWT_SECRET);
        // persist the token as 't' in cookie with expiry date
        res.cookie("t", token, {expire: new Date() + 9999});
        // return response with user and token to frontend
        const {_id, name, email, role} = user;
        return res.json({token, user: {_id, email, name, role}});
    });
};

exports.signout = (req, res) => {
    res.clearCookie("t");
    return res.json({message: "Wylogowano pomyślnie!"});
};

exports.requireSignin = expressJwt({
    // if the token is valid, express jwt appends the verified users id
    // in an auth ket to the request object
    secret: process.env.JWT_SECRET,
    userProperty: "auth"
});

// add forgotPassword and resetPassword methods
exports.forgotPassword = (req, res) => {
    if (!req.body) return res.status(400).json({ message: "Brak treści żądania" });
    if (!req.body.email)
        return res.status(400).json({ message: "Brak e-maila w treści żądania" });

    console.log("forgot password finding user with that email");
    const { email } = req.body;
    console.log("signin req.body", email);
    // find the user based on email
    User.findOne({ email }, (err, user) => {
        // if err or no user
        if (err || !user)
            return res.status("401").json({
                error: "Użytkownik z tym adresem e-mail nie istnieje!"
            });

        // generate a token with user id and secret
        const token = jwt.sign(
            { _id: user._id, iss: "NODEAPI" },
            process.env.JWT_SECRET
        );

        // email data
        const emailData = {
            from: "noreply@node-react.com",
            to: email,
            subject: "Instrukcje resetowania hasła",
            text: `Użyj poniższego linku, aby zresetować hasło: ${
                process.env.CLIENT_URL
            }/reset-password/${token}`,
            html: `<p>Użyj poniższego linku, aby zresetować hasło:</p> <p>${
                process.env.CLIENT_URL
            }/reset-password/${token}</p>`
        };

        return user.updateOne({ resetPasswordLink: token }, (err, success) => {
            if (err) {
                return res.json({ message: err });
            } else {
                sendEmail(emailData);
                return res.status(200).json({
                    message: `E-mail został wysłany na adres ${email}. Postępuj zgodnie z instrukcjami, aby zresetować hasło.`
                });
            }
        });
    });
};

// to allow user to reset password
// first you will find the user in the database with user's resetPasswordLink
// user model's resetPasswordLink's value must match the token
// if the user's resetPasswordLink(token) matches the incoming req.body.resetPasswordLink(token)
// then we got the right user

exports.resetPassword = (req, res) => {
    const { resetPasswordLink, newPassword } = req.body;

    User.findOne({ resetPasswordLink }, (err, user) => {
        // if err or no user
        if (err || !user)
            return res.status("401").json({
                error: "Invalid Link!"
            });

        const updatedFields = {
            password: newPassword,
            resetPasswordLink: ""
        };

        user = _.extend(user, updatedFields);
        user.updated = Date.now();

        user.save((err, result) => {
            if (err) {
                return res.status(400).json({
                    error: err
                });
            }
            res.json({
                message: `Gotowe! Teraz możesz zalogować się przy użyciu nowego hasła.`
            });
        });
    });
};